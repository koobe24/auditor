package pl.koobe24.auditor;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

/**
 * Created by tomas on 11.11.2016.
 */
public class FileParser {
    private File input;
    private Document document;
    private List<AuditRow> rowList;

    public FileParser(File input) {
        this.input = input;
    }

    public void initialize() throws IOException {
        document = Jsoup.parse(input, "UTF-8");
        clean();
        try {
            createDataRows();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void clean() {
        document = new Cleaner(Whitelist.relaxed()).clean(document);
        document.outputSettings().escapeMode(Entities.EscapeMode.base);

        try (BufferedWriter htmlWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("cleaned.html"), "UTF-8"))) {
            htmlWriter.write(document.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //todo pozmieniac na stale
    private void createDataRows() throws ParseException {
        rowList = new ArrayList<>();
        Elements rows = document.getElementsByTag("tr");
        ListIterator<Element> iterator = rows.listIterator(2);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd h:mm a");
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        while (iterator.hasNext()) {
            Element e = iterator.next();
            //todo moze pozniej jakos to ladniej?
            //todo np rownolegle ladowac?
            TransactionDetails transactionDetails = new TransactionDetails(dateFormat.parse(e.child(0).text()), e.child(1).text(), e.child(2).text(), e.child(3).text());

            TransactionAmounts transactionAmounts = new TransactionAmounts(e.child(4).text(), numberFormat.parse(parseNegativeDouble(e.child(5).text())).doubleValue(),
                    numberFormat.parse(e.child(6).text()).doubleValue(), numberFormat.parse(parseNegativeDouble(e.child(7).text())).doubleValue(), numberFormat.parse(e.child(8).text()).doubleValue(),
                    numberFormat.parse(e.child(9).text()).doubleValue());
            RunningBalance runningBalance = new RunningBalance(numberFormat.parse(e.child(10).text()).doubleValue(), numberFormat.parse(e.child(11).text()).doubleValue(),
                    numberFormat.parse(e.child(12).text()).doubleValue(), numberFormat.parse(e.child(13).text()).doubleValue(),
                    numberFormat.parse(e.child(14).text()).doubleValue(), numberFormat.parse(e.child(15).text()).doubleValue());
            AuditRow auditRow = new AuditRow(transactionDetails, transactionAmounts, runningBalance);
            rowList.add(auditRow);
        }

    }

    public List<AuditRow> getRowList() {
        return rowList;
    }

    private String parseNegativeDouble(String number) {
        if (number.contains("(") || number.contains(")")) {
            number = number.replace("(", "-");
            number = number.replace(")", "");
        }
        return number;
    }
}
