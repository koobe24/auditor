package pl.koobe24.auditor;

/**
 * Created by tomas on 11.11.2016.
 */
public class TransactionAmounts {
    private String currency;
    private Double amount;
    private Double accruedVpp;
    private Double accruedStarsCoin;
    private Double tMoney;
    private Double wMoney;

    public TransactionAmounts(String currency, Double amount, Double accruedVpp, Double accruedStarsCoin, Double tMoney, Double wMoney) {
        this.currency = currency;
        this.amount = amount;
        this.accruedVpp = accruedVpp;
        this.accruedStarsCoin = accruedStarsCoin;
        this.tMoney = tMoney;
        this.wMoney = wMoney;
    }

    public String getCurrency() {
        return currency;
    }

    public Double getAmount() {
        return amount;
    }

    public Double getAccruedVpp() {
        return accruedVpp;
    }

    public Double getAccruedStarsCoin() {
        return accruedStarsCoin;
    }

    public Double gettMoney() {
        return tMoney;
    }

    public Double getwMoney() {
        return wMoney;
    }

}
