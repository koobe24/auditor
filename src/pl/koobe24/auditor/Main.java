package pl.koobe24.auditor;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("layout.fxml"));
        primaryStage.setTitle("Auditor");
        primaryStage.setScene(new Scene(root, 600, 500));
        //todo stala
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
