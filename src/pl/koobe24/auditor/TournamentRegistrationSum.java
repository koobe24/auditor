package pl.koobe24.auditor;

import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * Created by tomas on 12.11.2016.
 */
public class TournamentRegistrationSum extends RecursiveTask<Integer> {
    private final int seqThreshold = 1000;
    private List<AuditRow> data;
    private int start, end;
    private TransactionDetails.Tournament tournament;


    public TournamentRegistrationSum(List<AuditRow> data, int s, int e, TransactionDetails.Tournament tournament) {
        this.data = data;
        start = s;
        end = e;
        this.tournament = tournament;
    }

    @Override
    protected Integer compute() {
        Integer sum = 0;
        if ((end - start) < seqThreshold) {
            for (int i = start; i < end; i++) {
                AuditRow row = data.get(i);
                if (row.getTransactionDetails().getActionEnum() == TransactionDetails.Action.TOURNAMENT_REGISTRATION) {
                    if (row.getTransactionDetails().getTournamentType() == tournament) {
                        sum += 1;
                    }
                }
            }
        } else {
            int middle = (start + end) / 2;
            TournamentRegistrationSum subTaskA = new TournamentRegistrationSum(data, start, middle, tournament);
            TournamentRegistrationSum subTaskB = new TournamentRegistrationSum(data, middle, end, tournament);
            subTaskA.fork();
            subTaskB.fork();

            sum = subTaskA.join() + subTaskB.join();
        }
        return sum;
    }

}
