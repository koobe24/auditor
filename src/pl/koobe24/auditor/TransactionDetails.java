package pl.koobe24.auditor;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tomas on 11.11.2016.
 */
public class TransactionDetails {
    private Date dateTime;
    //todo poprawic na enum?
    private String action;
    private String number;
    private String game;
    private Action actionEnum;
    private Tournament tournamentEnum;

    public TransactionDetails(Date dateTime, String action, String number, String game) {
        this.dateTime = dateTime;
        this.action = action;
        this.number = number;
        this.game = game;
        switch (action) {
            case "Buy Chips Reward":
                actionEnum = Action.BUY_CHIPS_REWARD;
                break;
            case "Cashout":
                actionEnum = Action.CASHOUT;
                break;
            case "Deposit (Player)":
                actionEnum = Action.DEPOSIT_PLAYER;
                break;
            case "ICE Challenge Reward (Cash)":
                actionEnum = Action.ICE_CHALLENGE_REWARD;
                break;
            case "Real Money Transfer Received":
                actionEnum = Action.REAL_MONEY_TRANSFER_RECEIVED;
                break;
            case "Real Money Transfer Sent":
                actionEnum = Action.REAL_MONEY_TRANSFER_SENT;
                break;
            case "StarsCoin Bundle":
                actionEnum = Action.STARSCOIN_BUNDLE;
                break;
            case "StarsCoin Store: Item Purchase (Multiple)":
                actionEnum = Action.STARS_COIN_STORE_ITEM_PURCHASE;
                break;
            case "StarsCoin Store: VIP Reward Purchased":
                actionEnum = Action.STARS_COIN_STORE_VIP_PURCHASE_REWARD;
                break;
            case "Tournament Registration":
                actionEnum = Action.TOURNAMENT_REGISTRATION;
                break;
            case "Tournament Won":
                actionEnum = Action.TOURNAMENT_WON;
                break;
            case "VPP earned (Tournament)":
                actionEnum = Action.VPP_EARNED_TOURNAMENT;
                break;
            case "VIP Status Set":
                actionEnum = Action.VIP_STATUS_SET;
                break;
            case "Tournament Unregistration":
                actionEnum = Action.TOURNAMENT_UNREGISTRATION;
                break;
            default:
                actionEnum = Action.OTHER;
        }

        if (actionEnum == Action.TOURNAMENT_REGISTRATION || actionEnum == Action.TOURNAMENT_WON || actionEnum == Action.TOURNAMENT_UNREGISTRATION) {
            setTournamentEnum();
        } else {
            tournamentEnum = Tournament.NONE;
        }
    }

    private void setTournamentEnum() {
        Pattern p = Pattern.compile("\\$([0]?.?[0-9]+).+SpinGo.+");
        Matcher mat = p.matcher(game);
        Pattern p2 = Pattern.compile("\\$([0]?.?[0-9]+).+Spin \\& Go");
        Matcher mat2 = p2.matcher(game);
        String find;
        if (mat.find()) {
            find = mat.group(1);
        } else if (mat2.find()) {
            find = mat2.group(1);
        } else {
            tournamentEnum = Tournament.OTHER;
            return;
        }
        switch (find) {
            //todo zmien to
            case "0.25":
                tournamentEnum = Tournament.POINT_TWENTY_FIVE;
                break;
            case "0.5":
                tournamentEnum = Tournament.HALF;
                break;
            case "1":
                tournamentEnum = Tournament.ONE;
                break;
            case "3":
                tournamentEnum = Tournament.THREE;
                break;
            case "5":
                tournamentEnum = Tournament.FIVE;
                break;
            case "7":
                tournamentEnum = Tournament.SEVEN;
                break;
            case "10":
                tournamentEnum = Tournament.TEN;
                break;
            case "15":
                tournamentEnum = Tournament.FIFTEEN;
                break;
            case "30":
                tournamentEnum = Tournament.THIRTY;
                break;
            case "60":
                tournamentEnum = Tournament.SIXTY;
                break;
            case "100":
                tournamentEnum = Tournament.HOUNDRED;
                break;
        }
    }

    public Date getDateTime() {
        return dateTime;
    }


    public String getNumber() {
        return number;
    }


    public String getGame() {
        return game;
    }


    public String getAction() {
        return action;
    }

    public Action getActionEnum() {
        return actionEnum;
    }

    public Tournament getTournamentType() {
        return tournamentEnum;
    }

    public enum Action {
        BUY_CHIPS_REWARD, CASHOUT, DEPOSIT_PLAYER, ICE_CHALLENGE_REWARD, REAL_MONEY_TRANSFER_RECEIVED, REAL_MONEY_TRANSFER_SENT, STARSCOIN_BUNDLE, STARS_COIN_STORE_ITEM_PURCHASE,
        STARS_COIN_STORE_VIP_PURCHASE_REWARD, TOURNAMENT_REGISTRATION, TOURNAMENT_WON, VIP_STATUS_SET, VPP_EARNED_TOURNAMENT, OTHER, TOURNAMENT_UNREGISTRATION;
    }

    public enum Tournament {
        POINT_TWENTY_FIVE(0.25), HALF(0.50), ONE(1.00), THREE(3.00), FIVE(5.00), SEVEN(7.00), TEN(10.00), FIFTEEN(15.00), THIRTY(30.00),
        SIXTY(60.00), HOUNDRED(100.00), NONE(Double.MIN_VALUE), OTHER(0);
        private final double amount;

        Tournament(double amount) {
            this.amount = amount;
        }

        public double getAmount() {
            return amount;
        }

        public String getAmountString() {
            return "$" + amount;
        }
    }
}
