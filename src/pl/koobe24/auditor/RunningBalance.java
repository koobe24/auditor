package pl.koobe24.auditor;

/**
 * Created by tomas on 11.11.2016.
 */
public class RunningBalance {
    private Double balance;
    private Double totalMonthlyVppAfterThisTransaction;
    private Double totalYearlyVppAfterThisTransaction;
    private Double totalAccruedStarscoinAfterThisTransaction;
    private Double tMoney;
    private Double wMoney;

    public RunningBalance(Double balance, Double totalMonthlyVppAfterThisTransaction, Double totalYearlyVppAfterThisTransaction,
                          Double totalAccruedStarscoinAfterThisTransaction, Double tMoney, Double wMoney) {
        this.balance = balance;
        this.totalMonthlyVppAfterThisTransaction = totalMonthlyVppAfterThisTransaction;
        this.totalYearlyVppAfterThisTransaction = totalYearlyVppAfterThisTransaction;
        this.totalAccruedStarscoinAfterThisTransaction = totalAccruedStarscoinAfterThisTransaction;
        this.tMoney = tMoney;
        this.wMoney = wMoney;
    }

    public Double getBalance() {
        return balance;
    }

    public Double getTotalMonthlyVppAfterThisTransaction() {
        return totalMonthlyVppAfterThisTransaction;
    }


    public Double getTotalYearlyVppAfterThisTransaction() {
        return totalYearlyVppAfterThisTransaction;
    }

    public Double getTotalAccruedStarscoinAfterThisTransaction() {
        return totalAccruedStarscoinAfterThisTransaction;
    }

    public Double gettMoney() {
        return tMoney;
    }

    public Double getwMoney() {
        return wMoney;
    }

}
