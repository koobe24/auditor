package pl.koobe24.auditor;

import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * Created by tomas on 12.11.2016.
 */
public class SpinGoProfit extends RecursiveTask<Double> {

    private final int seqThreshold = 1000;
    private List<AuditRow> data;
    private int start, end;

    public SpinGoProfit(List<AuditRow> data, int start, int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Double compute() {
        Double sum = 0.0;
        if ((end - start) < seqThreshold) {
            for (int i = start; i < end; i++) {
                AuditRow row = data.get(i);
                switch (row.getTransactionDetails().getActionEnum()) {
                    case TOURNAMENT_WON:
                    case TOURNAMENT_REGISTRATION:
                    case TOURNAMENT_UNREGISTRATION:
                        if (row.getTransactionDetails().getTournamentType() != TransactionDetails.Tournament.OTHER &&
                                row.getTransactionDetails().getTournamentType() != TransactionDetails.Tournament.NONE) {
                            sum += row.getTransactionAmounts().getAmount();
                        }
                        break;
                }
            }
        } else {
            int middle = (start + end) / 2;
            SpinGoProfit subTaskA = new SpinGoProfit(data, start, middle);
            SpinGoProfit subTaskB = new SpinGoProfit(data, middle, end);
            subTaskA.fork();
            subTaskB.fork();

            sum = subTaskA.join() + subTaskB.join();
        }
        return sum;
    }
}
