package pl.koobe24.auditor;

import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * Created by tomas on 11.11.2016.
 */
public class StarsCoinIncome extends RecursiveTask<Double> {

    private final int seqThreshold = 1000;
    private List<AuditRow> data;
    private int start, end;

    public StarsCoinIncome(List<AuditRow> data, int start, int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Double compute() {
        Double sum = 0.0;
        if ((end - start) < seqThreshold) {
            for (int i = start; i < end; i++) {
                AuditRow row = data.get(i);
                switch (row.getTransactionDetails().getActionEnum()) {
                    case STARSCOIN_BUNDLE:
                        sum += row.getTransactionAmounts().getAccruedStarsCoin();
                        break;
                }
            }
        } else {
            int middle = (start + end) / 2;
            StarsCoinIncome subTaskA = new StarsCoinIncome(data, start, middle);
            StarsCoinIncome subTaskB = new StarsCoinIncome(data, middle, end);
            subTaskA.fork();
            subTaskB.fork();

            sum = subTaskA.join() + subTaskB.join();
        }
        return sum;
    }
}
