package pl.koobe24.auditor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by tomas on 11.11.2016.
 */
public class Summary {

    class MutableInt {
        int value = 1;

        public void increment() {
            ++value;
        }

        public int get() {
            return value;
        }
    }

    private List<AuditRow> auditRowList;
    private Double starsCoinIncome;
    private Integer tournamentsPlayedSum;
    private Integer tournamentsWonSum;
    private Map<TransactionDetails.Tournament, Integer> tournamentsPlayedMap;
    private Map<TransactionDetails.Tournament, Integer> tournamentsWonMap;
    private Map<String, MutableInt> otherPlayed;
    private Double spinGoProfit;


    public Summary(List<AuditRow> auditRowList) {
        this.auditRowList = auditRowList;
    }

    public void countTotals() {
        ForkJoinPool fjp = new ForkJoinPool(Runtime.getRuntime().availableProcessors());

        StarsCoinIncome starsCoinIncomeTask = new StarsCoinIncome(auditRowList, 0, auditRowList.size());
        starsCoinIncome = fjp.invoke(starsCoinIncomeTask);

        tournamentsPlayedMap = new TreeMap<>();
        tournamentsWonMap = new TreeMap<>();
        tournamentsPlayedSum = 0;
        tournamentsWonSum = 0;
        for (TransactionDetails.Tournament t : TransactionDetails.Tournament.values()) {
            TournamentRegistrationSum task = new TournamentRegistrationSum(auditRowList, 0, auditRowList.size(), t);
            int howMuch = fjp.invoke(task);
            tournamentsPlayedMap.put(t, howMuch);

            TournamentWonSum task2 = new TournamentWonSum(auditRowList, 0, auditRowList.size(), t);
            int howMuch2 = fjp.invoke(task2);
            tournamentsWonMap.put(t, howMuch2);

            if (t == TransactionDetails.Tournament.OTHER || t == TransactionDetails.Tournament.NONE)
                continue;
            tournamentsPlayedSum += howMuch;
            tournamentsWonSum += howMuch2;
        }

        SpinGoProfit task = new SpinGoProfit(auditRowList, 0, auditRowList.size());
        spinGoProfit = fjp.invoke(task);

        //todo pomysl jak usprawnic
        otherPlayed = new HashMap<>();
        auditRowList.forEach(c -> {
            if (c.getTransactionDetails().getTournamentType() == TransactionDetails.Tournament.OTHER) {
                String word = c.getTransactionDetails().getGame();
                MutableInt count = otherPlayed.get(word);
                if (count == null) {
                    otherPlayed.put(word, new MutableInt());
                } else {
                    count.increment();
                }
            }
        });
    }

    public Double getStarsCoinIncome() {
        return starsCoinIncome;
    }

    public Map<String, MutableInt> getOtherPlayed() {
        return otherPlayed;
    }

    public Integer getTournamentsPlayedSum() {
        return tournamentsPlayedSum;
    }

    public Integer getTournamentsWonSum() {
        return tournamentsWonSum;
    }

    public Map<TransactionDetails.Tournament, Integer> getTournamentsPlayedMap() {
        return tournamentsPlayedMap;
    }

    public Map<TransactionDetails.Tournament, Integer> getTournamentsWonMap() {
        return tournamentsWonMap;
    }

    public Double getSpinGoProfit() {
        return spinGoProfit;
    }
}
