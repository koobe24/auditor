package pl.koobe24.auditor;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

public class Controller implements Initializable {
    @FXML
    private TextField filePathField;

    @FXML
    private Button fileChooseButton;

    @FXML
    private TableView<AuditRow> dataTableView;

    @FXML
    private GridPane summaryGridPane;

    private FileChooser fileChooser;

    private final String FILE_CHOOSER_PATH = "file_chooser_path";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("html files (*.html)", "*.html");
        fileChooser.getExtensionFilters().add(filter);
        fileChooseButton.setOnAction(event -> {
            Preferences prefs = Preferences.userNodeForPackage(pl.koobe24.auditor.Main.class);
            String defaultValue = prefs.get(FILE_CHOOSER_PATH, "");
            if (!defaultValue.equals("")) {
                fileChooser.setInitialDirectory(new File(defaultValue));
            }
            File file = fileChooser.showOpenDialog(fileChooseButton.getScene().getWindow());
            if (file != null) {
                prefs.put(FILE_CHOOSER_PATH, file.getParentFile().getAbsolutePath());
                dataTableView.getColumns().clear();
                dataTableView.getItems().clear();
                dataTableView.setTableMenuButtonVisible(true);
                filePathField.setText(file.getAbsolutePath());
                FileParser parser = new FileParser(file);
                try {
                    parser.initialize();
                    Summary sum = new Summary(parser.getRowList());
                    sum.countTotals();
                    summaryGridPane.getChildren().clear();
                    summaryGridPane.add(new Label("Total number of Spin&Gos played:"), 0, 0);
                    summaryGridPane.add(new Label(sum.getTournamentsPlayedSum().toString()), 1, 0);
                    summaryGridPane.add(new Label("Count by stakes:"), 0, 1);
                    int i = 2;
                    for (Map.Entry<TransactionDetails.Tournament, Integer> e : sum.getTournamentsPlayedMap().entrySet()) {
                        Integer value = e.getValue();
                        if (value != 0 && e.getKey()!= TransactionDetails.Tournament.OTHER) {
                            summaryGridPane.add(new Label(e.getKey().getAmountString()), 0, i);
                            summaryGridPane.add(new Label(value.toString()), 1, i++);
                        }
                    }
                    double spinGoProfit = sum.getSpinGoProfit();
                    summaryGridPane.add(new Label("Total profit from Spin&Gos:"), 0, i);
                    summaryGridPane.add(new Label("$" + spinGoProfit), 1, i++);
                    double rakeback = (sum.getStarsCoinIncome() / 100);
                    summaryGridPane.add(new Label("Total rakeback from StarsCoins:"), 0, i);
                    summaryGridPane.add(new Label("$" + rakeback), 1, i++);
                    double totalProfit = spinGoProfit + rakeback;
                    summaryGridPane.add(new Label("Total profit+RB to split:"), 0, i);
                    summaryGridPane.add(new Label("$" + totalProfit), 1, i++);
                    summaryGridPane.add(new Separator(Orientation.HORIZONTAL), 0, i++);
                    if (!sum.getOtherPlayed().isEmpty()) {
                        summaryGridPane.add(new Label("Other games played:"), 0, i++);
                        for (Map.Entry<String, Summary.MutableInt> e : sum.getOtherPlayed().entrySet()) {
                            summaryGridPane.add(new Label(e.getKey()), 0, i);
                            summaryGridPane.add(new Label(e.getValue().value + ""), 1, i++);
                        }
                    }
                    Button saveButton = new Button("Save to .txt");
                    summaryGridPane.add(saveButton, 0, i);
                    saveButton.setOnAction(e -> {
                        FileChooser saveFileChooser = new FileChooser();
                        FileChooser.ExtensionFilter saveFilter = new FileChooser.ExtensionFilter("txt files (*.txt)", "*.txt");
                        saveFileChooser.getExtensionFilters().add(saveFilter);
                        File saveFile = saveFileChooser.showSaveDialog(saveButton.getScene().getWindow());
                        if (saveFile != null) {
                            saveFile(saveFile, sum);
                        }
                    });
                    Platform.runLater(() -> {
                        for (AuditRow row : parser.getRowList()) {
                            dataTableView.getItems().add(row);
                        }
                        setupColumns();
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void saveFile(File file, Summary sum) {
        try (FileWriter writer = new FileWriter(file)) {
            writer.write("Total number of Spin&Gos played: " + sum.getTournamentsPlayedSum() +System.lineSeparator());
            writer.write("Count by stakes:"+System.lineSeparator());
            for (Map.Entry<TransactionDetails.Tournament, Integer> e : sum.getTournamentsPlayedMap().entrySet()) {
                Integer value = e.getValue();
                if (value != 0 && e.getKey()!= TransactionDetails.Tournament.OTHER) {
                    writer.write(e.getKey().getAmountString() + ": " + e.getValue() + System.lineSeparator());
                }
            }
            writer.write("Total profit from Spin&Gos: $" + sum.getSpinGoProfit() + System.lineSeparator());
            double rakeback = sum.getStarsCoinIncome() / 100;
            writer.write("Total rakeback from StarsCoin: $" + rakeback + System.lineSeparator());
            writer.write("Total profit+RB to split: $" + (sum.getSpinGoProfit() + rakeback) + System.lineSeparator());
            if (!sum.getOtherPlayed().isEmpty()) {
                writer.write("##########################"+System.lineSeparator()+"Other games played:");
                for (Map.Entry<String, Summary.MutableInt> e : sum.getOtherPlayed().entrySet()) {
                    writer.write(System.lineSeparator()+e.getKey() + ": " + e.getValue().get() + System.lineSeparator());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setupColumns() {
        TableColumn<AuditRow, Date> dateTableColumn = new TableColumn<>("Date/Time");
        dateTableColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getTransactionDetails().getDateTime()));

        TableColumn<AuditRow, String> actionColumn = new TableColumn<>("Action");
        actionColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getTransactionDetails().getAction()));

        TableColumn<AuditRow, String> numberColumn = new TableColumn<>("Table Name / Player / Tournament #");
        numberColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue().getTransactionDetails().getNumber()));

        TableColumn<AuditRow, String> gameColumn = new TableColumn<>("Game");
        gameColumn.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getTransactionDetails().getGame()));

        TableColumn<AuditRow, String> currencyColumn = new TableColumn<>("Account Currency");
        currencyColumn.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getTransactionAmounts().getCurrency()));

        TableColumn<AuditRow, Double> amountColumn = new TableColumn<>("Amount");
        amountColumn.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getTransactionAmounts().getAmount()));

        TableColumn<AuditRow, Double> accruedVPPColumn = new TableColumn<>("Accrued VPP");
        accruedVPPColumn.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getTransactionAmounts().getAccruedVpp()));

        TableColumn<AuditRow, Double> accruedStarscoin = new TableColumn<>("Accrued StarsCoin");
        accruedStarscoin.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getTransactionAmounts().getAccruedStarsCoin()));

        TableColumn<AuditRow, Double> tMoneyAmounts = new TableColumn<>("T Money");
        tMoneyAmounts.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getTransactionAmounts().gettMoney()));

        TableColumn<AuditRow, Double> wMoneyAmounts = new TableColumn<>("W Money");
        wMoneyAmounts.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getTransactionAmounts().getwMoney()));

        TableColumn<AuditRow, Double> balanceColumn = new TableColumn<>("Balance");
        balanceColumn.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getRunningBalance().getBalance()));

        TableColumn<AuditRow, Double> TMVPPATColumn = new TableColumn<>("Total Monthly VPP After this Transaction");
        TMVPPATColumn.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getRunningBalance().getTotalMonthlyVppAfterThisTransaction()));

        TableColumn<AuditRow, Double> TYVPPATColumn = new TableColumn<>("Total Yearly VPP After this Transaction");
        TYVPPATColumn.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getRunningBalance().getTotalYearlyVppAfterThisTransaction()));

        TableColumn<AuditRow, Double> TASCATColumn = new TableColumn<>("Total Accrued StarsCoin After this Transaction");
        TASCATColumn.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getRunningBalance().getTotalAccruedStarscoinAfterThisTransaction()));

        TableColumn<AuditRow, Double> tMoneyBalance = new TableColumn<>("T Money");
        tMoneyBalance.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getRunningBalance().gettMoney()));

        TableColumn<AuditRow, Double> wMoneyBalance = new TableColumn<>("W Money");
        wMoneyBalance.setCellValueFactory(c -> new ReadOnlyObjectWrapper<>(c.getValue().getRunningBalance().getwMoney()));

        TableColumn transactionDetailsColumn = new TableColumn("Transaction Details");
        transactionDetailsColumn.getColumns().addAll(dateTableColumn, actionColumn, numberColumn, gameColumn);

        TableColumn transactionAmountsColumn = new TableColumn("Transaction Amounts");
        transactionAmountsColumn.getColumns().addAll(currencyColumn, amountColumn, accruedVPPColumn, accruedStarscoin, tMoneyAmounts,
                wMoneyAmounts);

        TableColumn runningBalanceColumn = new TableColumn("Running Balance");
        runningBalanceColumn.getColumns().addAll(balanceColumn, TMVPPATColumn, TYVPPATColumn, TASCATColumn, tMoneyBalance, wMoneyBalance);

        dataTableView.getColumns().addAll(transactionDetailsColumn, transactionAmountsColumn, runningBalanceColumn);
    }
}
