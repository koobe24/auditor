package pl.koobe24.auditor;

import java.util.Formatter;

/**
 * Created by tomas on 11.11.2016.
 */
public class AuditRow {
    private TransactionDetails transactionDetails;
    private TransactionAmounts transactionAmounts;
    private RunningBalance runningBalance;

    public AuditRow(TransactionDetails transactionDetails, TransactionAmounts transactionAmounts, RunningBalance runningBalance) {
        this.transactionDetails = transactionDetails;
        this.transactionAmounts = transactionAmounts;
        this.runningBalance = runningBalance;
    }

    public TransactionDetails getTransactionDetails() {
        return transactionDetails;
    }

    public TransactionAmounts getTransactionAmounts() {
        return transactionAmounts;
    }

    public RunningBalance getRunningBalance() {
        return runningBalance;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        Formatter formatter = new Formatter(builder);
        formatter.format("Transaction details:\n");
        formatter.format("Date/Time: %1$tY/%1$tm/%1$td %1$tl:%1$tM %1$tp; Action: %2$s; Table Name/Player/Tournament# %3$s; Game: %4$s;\n", transactionDetails.getDateTime(), transactionDetails.getAction(),
                transactionDetails.getNumber(), transactionDetails.getGame());
        formatter.format("Individual Transaction Amounts:\n");
        formatter.format("Account Currency: %s; Amount: %f; Accrued VPP: %f, Accrued StarsCoin: %f; T Money: %f; W Money: %f;\n", transactionAmounts.getCurrency(),
                transactionAmounts.getAmount(), transactionAmounts.getAccruedVpp(), transactionAmounts.getAccruedStarsCoin(), transactionAmounts.gettMoney(), transactionAmounts.getwMoney());
        formatter.format("Running balance:\n");
        formatter.format("Balance: %f; TMVPPAT: %f; TYVPPAT: %f; TASCAT: %f; T Money: %f; W Money: %f\n", runningBalance.getBalance(), runningBalance.getTotalMonthlyVppAfterThisTransaction(),
                runningBalance.getTotalYearlyVppAfterThisTransaction(), runningBalance.getTotalAccruedStarscoinAfterThisTransaction(), runningBalance.gettMoney(), runningBalance.getwMoney());
        return builder.toString();
    }
}
